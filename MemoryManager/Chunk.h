#pragma once

#define OVERRIDE_DEFAULT_ALLOCATOR false

namespace FastMemoryManager {

	typedef unsigned char block_t;
	typedef size_t block_size_t;

	struct Chunk
	{
	public:
		void Init(block_size_t i_blockSize, block_t i_blocksNum);
		void* Allocate(block_size_t i_blockSize);
		void Deallocate(void* i_p, block_size_t i_blockSize);
		bool IsFull();
		bool IsEmpty(block_t i_blocksNum);
		bool IsInRange(void* i_p, block_size_t i_blockSize, block_t i_blocksNum);
	private:
		unsigned char m_firstAvailableBlock;
		unsigned char m_blocksAvailable;
		unsigned char* m_blocks;
	};

}