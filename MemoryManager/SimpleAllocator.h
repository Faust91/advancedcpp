#pragma once

#include <stdlib.h>
#include <new>

namespace FastMemoryManager {

	template <class T> class SimpleAllocator {
	public:
		typedef T value_type;

		SimpleAllocator() noexcept;
		template <class U> SimpleAllocator(const SimpleAllocator<U>&) noexcept;

		template <class U> bool operator==(const SimpleAllocator<U>&) const noexcept;
		template <class U> bool operator!=(const SimpleAllocator<U>&) const noexcept;

		T* allocate(const size_t) const;
		void deallocate(T* const, size_t) const noexcept;
	};

	template <class T>
	SimpleAllocator<T>::SimpleAllocator() noexcept
	{}
	template <class T>
	template <class U>
	SimpleAllocator<T>::SimpleAllocator(const SimpleAllocator<U>&) noexcept
	{}

	template <class T>
	template <class U>
	bool SimpleAllocator<T>::operator==(const SimpleAllocator<U>&) const noexcept
	{
		return true;
	}
	template <class T>
	template <class U>
	bool SimpleAllocator<T>::operator!=(const SimpleAllocator<U>&) const noexcept
	{
		return false;
	}

	template <class T>
	T* SimpleAllocator<T>::allocate(const size_t s) const
	{
		if (s == 0) { return nullptr; }
		if (s > static_cast<size_t>(-1) / sizeof(T)) {
			throw std::bad_array_new_length();
		}
		void* const p = malloc(s * sizeof(T));
		if (!p) { throw std::bad_alloc(); }
		return static_cast<T*>(p);
	}
	template <class T>
	void SimpleAllocator<T>::deallocate(T* const p, size_t /*s*/) const noexcept
	{
		free(p);
	}


}