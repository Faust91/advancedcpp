#include "Main.h"
#include <memory.h>
#include <iostream>

int main(int argc, char* argv[]) {

	FastMemoryManager::MemoryManager mM = FastMemoryManager::MemoryManager();

	int* intPtr = new int();

	std::cout << "intPtr address     : " << intPtr << std::endl;
	*intPtr = 1;
	std::cout << "intPtr value       : " << *intPtr << std::endl;

	size_t sizeOfInt = sizeof(int);

	int* smallObjPtr = static_cast<int*>(mM.MM_NEW(sizeOfInt));

	std::cout << "smallObjPtr address: " << smallObjPtr << std::endl;
	*smallObjPtr = 2;
	std::cout << "smallObjPtr value  : " << *smallObjPtr << std::endl;

	return 0;
}