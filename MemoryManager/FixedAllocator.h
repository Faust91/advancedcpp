#pragma once
#include "Chunk.h"
#include "SimpleAllocator.h"
#include <vector>

namespace FastMemoryManager {

	class FixedAllocator
	{
#if OVERRIDE_DEFAULT_ALLOCATOR
		using Chunks = std::vector < Chunk, SimpleAllocator<Chunk> >;
#else
		using Chunks = std::vector<Chunk>;
#endif

	public:
		explicit FixedAllocator(block_size_t i_blockSize, block_t i_chunkSize);
		void* Allocate();
		bool Deallocate(void* i_p);
		block_size_t BlockSize();
	private:
		block_size_t m_blockSize;
		block_t m_blocksNum;
		Chunks m_chunks;
		Chunk* m_allocChunk;
		Chunk* m_deallocChunk;
	};

}