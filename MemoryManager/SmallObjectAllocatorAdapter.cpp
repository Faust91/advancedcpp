#include "SmallObjectAllocatorAdapter.h"

#include <stdlib.h>
#include <new>

namespace {
	constexpr size_t CHUNK_SIZE = 1024;
}

template <class T>
FastMemoryManager::SmallObjectAllocatorAdapter<T>::SmallObjectAllocatorAdapter() noexcept : SmallObjectAllocator{ CHUNK_SIZE }
{}
template <class T>
FastMemoryManager::SmallObjectAllocatorAdapter<T>::SmallObjectAllocatorAdapter(SmallObjectAllocator& aSOA) noexcept : SmallObjectAllocator{ aSOA }
{}
template <class T>
template <class U>
FastMemoryManager::SmallObjectAllocatorAdapter<T>::SmallObjectAllocatorAdapter(const FastMemoryManager::SmallObjectAllocatorAdapter<U>& aSOAA) noexcept : SmallObjectAllocator{ aSOAA.GetSOA() }
{}

template <class T>
template <class U>
bool FastMemoryManager::SmallObjectAllocatorAdapter<T>::operator==(const FastMemoryManager::SmallObjectAllocatorAdapter<U>&) const noexcept
{
	return true;
}
template <class T>
template <class U>
bool FastMemoryManager::SmallObjectAllocatorAdapter<T>::operator!=(const FastMemoryManager::SmallObjectAllocatorAdapter<U>&) const noexcept
{
	return false;
}

template <class T>
T* FastMemoryManager::SmallObjectAllocatorAdapter<T>::allocate(const size_t s) const
{
	mySOA.Allocate(s * sizeof(T));
}
template <class T>
void FastMemoryManager::SmallObjectAllocatorAdapter<T>::deallocate(T* const p, size_t s) const noexcept
{
	mySOA.Deallocate(p, s * sizeof(T));
}
