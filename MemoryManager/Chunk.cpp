#include "Chunk.h"
#include <assert.h>
#include <stdlib.h>

void FastMemoryManager::Chunk::Init(block_size_t i_blockSize, block_t i_blocksNum)
{
	// alloc data
#if OVERRIDE_DEFAULT_ALLOCATOR
	m_blocks = static_cast<unsigned char*>(malloc(sizeof(block_t) * i_blockSize * i_blocksNum));
#else
	m_blocks = new block_t[i_blockSize * i_blocksNum];
#endif
	// init first available block index
	m_firstAvailableBlock = 0;
	// init available blocks counter
	m_blocksAvailable = i_blocksNum;
	// init free list
	block_t i = 0;
	block_t* p = m_blocks;
	for (; i < i_blocksNum; p += i_blockSize) {
		*p = ++i;
	}
}

void* FastMemoryManager::Chunk::Allocate(block_size_t i_blockSize)
{
	// availability check
	if (!m_blocksAvailable) {
		return nullptr;
	}
	// pick first available block
	block_t* block = m_blocks + (i_blockSize * m_firstAvailableBlock);
	// free list update
	m_firstAvailableBlock = *block;
	// blocksAvailable decrement
	--m_blocksAvailable;

	return block;
}

void FastMemoryManager::Chunk::Deallocate(void* i_p, block_size_t i_blockSize)
{
	// start position check
	assert(i_p >= m_blocks);
	// cast to blockType*
	block_t* block = static_cast<block_t*>(i_p);
	// alignment check
	assert((block - m_blocks) % i_blockSize == 0);
	// free list update
	*block = m_firstAvailableBlock;
	// address to index conversion and firstAvailableBlock update
	m_firstAvailableBlock = static_cast<block_t>((block - m_blocks) / i_blockSize);
	// truncate check
	assert(m_firstAvailableBlock == (block - m_blocks) / i_blockSize);
	// blocksAvailable increment
	++m_blocksAvailable;
}

bool FastMemoryManager::Chunk::IsFull() {
	return m_blocksAvailable == 0;
}

bool FastMemoryManager::Chunk::IsEmpty(block_t i_blocksNum)
{
	return m_blocksAvailable == i_blocksNum;
}

bool FastMemoryManager::Chunk::IsInRange(void* i_p, block_size_t i_blockSize, block_t i_blocksNum)
{
	return (i_p >= m_blocks && i_p < (m_blocks + (i_blockSize * i_blocksNum)));
}
