#pragma once
#include "SmallObjectAllocator.h"

namespace FastMemoryManager {

	template <class T> class SmallObjectAllocatorAdapter {
	public:
		SmallObjectAllocatorAdapter() noexcept;
		explicit SmallObjectAllocatorAdapter(SmallObjectAllocator&) noexcept;
		template <class U> SmallObjectAllocatorAdapter(const SmallObjectAllocatorAdapter<U>&) noexcept;

		template <class U> bool operator==(const SmallObjectAllocatorAdapter<U>&) const noexcept;
		template <class U> bool operator!=(const SmallObjectAllocatorAdapter<U>&) const noexcept;

		T* allocate(const size_t) const;
		void deallocate(T* const, size_t) const noexcept;

		inline public SmallObjectAllocator& GetSOA() {
			return mySOA;
		}
	private:
		SmallObjectAllocator& mySOA;
	};

}