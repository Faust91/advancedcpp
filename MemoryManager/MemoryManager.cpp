#include "MemoryManager.h"
#include <memory.h>
#include <iostream>

#define CHUNK_SIZE 128
#define MAX_SMALL_OBJECT_SIZE 64

FastMemoryManager::MemoryManager::MemoryManager() :m_SOA{ block_t(CHUNK_SIZE) }
{
}

FastMemoryManager::MemoryManager::~MemoryManager()
{
}

void* FastMemoryManager::MemoryManager::MM_NEW(size_t i_size)
{
	std::cout << "MM_NEW" << std::endl;
#if OVERRIDE_DEFAULT_ALLOCATOR
	SimpleAllocator<char> sA = SimpleAllocator<char>();
	return i_size > MAX_SMALL_OBJECT_SIZE ? sA.allocate(i_size) : m_SOA.Allocate(i_size);
#else
	return i_size > MAX_SMALL_OBJECT_SIZE ? ::operator new(i_size) : m_SOA.Allocate(i_size);
#endif
}

void* FastMemoryManager::MemoryManager::MM_NEW_A(size_t i_size, size_t i_length)
{
	std::cout << "MM_NEW_A" << std::endl;
#if OVERRIDE_DEFAULT_ALLOCATOR
	SimpleAllocator<char> sA = SimpleAllocator<char>();
	return i_size > MAX_SMALL_OBJECT_SIZE ? sA.allocate(i_size * i_length) : m_SOA.Allocate(i_size * i_length);
#else
	return i_size > MAX_SMALL_OBJECT_SIZE ? ::operator new[](i_size) : m_SOA.Allocate(i_size * i_length);
#endif
}

void FastMemoryManager::MemoryManager::MM_DELETE(void* i_p, size_t i_size)
{
	std::cout << "MM_DELETE" << std::endl;
#if OVERRIDE_DEFAULT_ALLOCATOR
	SimpleAllocator<void> sA = SimpleAllocator<void>();
	return i_size > MAX_SMALL_OBJECT_SIZE ? sA.deallocate(i_p, i_size) : m_SOA.Deallocate(i_p, i_size);
#else
	return i_size > MAX_SMALL_OBJECT_SIZE ? ::operator delete(i_p) : m_SOA.Deallocate(i_p, i_size);
#endif
}

void FastMemoryManager::MemoryManager::MM_DELETE_A(void* i_p, size_t i_size, size_t i_length)
{
	std::cout << "MM_DELETE_A" << std::endl;
#if OVERRIDE_DEFAULT_ALLOCATOR
	SimpleAllocator<void> sA = SimpleAllocator<void>();
	return i_size > MAX_SMALL_OBJECT_SIZE ? sA.deallocate(i_p, i_size * i_length) : m_SOA.Deallocate(i_p, i_size * i_length);
#else
	return i_size > MAX_SMALL_OBJECT_SIZE ? ::operator delete[](i_p) : m_SOA.Deallocate(i_p, i_size * i_length);
#endif
}

void* FastMemoryManager::MemoryManager::MM_ALLOC(size_t i_size)
{
	std::cout << "MM_ALLOC" << std::endl;
	return i_size > MAX_SMALL_OBJECT_SIZE ? ::malloc(i_size) : m_SOA.Allocate(i_size);
}

void FastMemoryManager::MemoryManager::MM_FREE(void* i_p, size_t i_size)
{
	std::cout << "MM_FREE" << std::endl;
	return i_size > MAX_SMALL_OBJECT_SIZE ? ::free(i_p) : m_SOA.Deallocate(i_p, i_size);
}

#if OVERRIDE_DEFAULT_ALLOCATOR
void* operator new(std::size_t n)
{
	void* p = fmm.MM_NEW(n);
	fmm.m_awfulSizeMap[p] = n;
	return p;
}
void operator delete(void* p)
{
	size_t s = fmm.m_awfulSizeMap[p];
	fmm.MM_DELETE(p, s);
	fmm.m_awfulSizeMap.erase(p);
}
#endif