#pragma once

#include "FixedAllocator.h"
#include <vector>
#include "SimpleAllocator.h"

namespace FastMemoryManager {

	class SmallObjectAllocator
	{
#if OVERRIDE_DEFAULT_ALLOCATOR
		using FixedAllocators = std::vector < FixedAllocator, SimpleAllocator<Chunk> >;
#else
		using FixedAllocators = std::vector<FixedAllocator>;
#endif

	public:
		SmallObjectAllocator(block_t i_chunkSize);
		void* Allocate(block_size_t i_numBytes);
		void Deallocate(void* i_p, size_t i_size);
	private:
		// Fixed allocators.
		FixedAllocators m_pool;
		// Last Fixed Allocator used for allocation.
		FixedAllocator* m_allocFA;
		// Last Fixed Allocator used for deallocation.
		FixedAllocator* m_deallocFA;
		// Length in bytes of each Chunk object.
		block_t m_chunkSize;
	};

}