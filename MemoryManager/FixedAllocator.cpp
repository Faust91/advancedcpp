#include "FixedAllocator.h"
#include <assert.h>

FastMemoryManager::FixedAllocator::FixedAllocator(block_size_t i_blockSize, block_t i_chunkSize) :
	m_blockSize{ i_blockSize }, m_blocksNum{ i_chunkSize }, m_allocChunk{ nullptr }, m_deallocChunk{ nullptr }
{}

void* FastMemoryManager::FixedAllocator::Allocate()
{
	// alloc_chunk full or not set (need for a new one)
	if (m_allocChunk == nullptr || m_allocChunk->IsFull()) {
		// get a chunk iterator
		Chunks::iterator i = m_chunks.begin();
		// move i to the first chunk that is not full
		for (; i != m_chunks.end() && i->IsFull(); ++i) {}
		// no free chunk found
		if (i == m_chunks.end()) {
			// create and add a new chunk to the chunks set
			//m_chunks.reserve(m_chunks.size() + 1);
			Chunk chunk;
			chunk.Init(m_blockSize, m_blocksNum);
			m_chunks.push_back(chunk);
			// update alloc and dealloc chunks
			m_allocChunk = &m_chunks.back();
			m_deallocChunk = &m_chunks.back();
		}
		else {
			// found a chunk
			m_allocChunk = &*i;
		}
	}
	// be sure there is a "not full" - alloc chunk now
	assert(m_allocChunk != nullptr);
	assert(!m_allocChunk->IsFull());
	// allocate a block into the alloc chunk and return it
	return m_allocChunk->Allocate(m_blockSize);
}

bool FastMemoryManager::FixedAllocator::Deallocate(void* i_p)
{
	// dealloc_chunk empty or not set or pointer not in range (need to find the correct one)
	if (m_deallocChunk == nullptr || m_deallocChunk->IsEmpty(m_blocksNum) || !m_deallocChunk->IsInRange(i_p, m_blockSize, m_blocksNum)) {
		// get an chunk iterator
		Chunks::iterator i = m_chunks.begin();
		// move i to correct chunk
		for (; i != m_chunks.end() && i->IsEmpty(m_blocksNum) && !i->IsInRange(i_p, m_blockSize, m_blocksNum); ++i) {}
		// currect chunk not found
		if (i == m_chunks.end()) {
			return false;
		}
		else {
			// correct chunk found
			m_deallocChunk = &*i;
		}
	}
	// be sure there is a "not full" - alloc chunk now
	assert(m_deallocChunk != nullptr);
	assert(!m_deallocChunk->IsEmpty(m_blocksNum));
	assert(m_deallocChunk->IsInRange(i_p, m_blockSize, m_blocksNum));
	// deallocate the block
	m_deallocChunk->Deallocate(i_p, m_blockSize);
	// if empty clean and resize the vector
	if (m_deallocChunk->IsEmpty(m_blocksNum)) {
		Chunks::iterator i = m_chunks.begin();
		// skip to the deallocChunk
		for (; i != m_chunks.end() && &*i != m_deallocChunk; ++i) {}
		// found
		if (i != m_chunks.end()) {
			// invalid dealloc_chunk pointer
			m_deallocChunk = nullptr;
			// remove chunk
			m_chunks.erase(i);
			// at least 2 empty chunks
			if (m_chunks.capacity() - m_chunks.size() >= 2) {
				// resize the vector
				m_chunks.resize(m_chunks.size());
			}
		}
	}
	return true;
}

FastMemoryManager::block_size_t FastMemoryManager::FixedAllocator::BlockSize()
{
	return m_blockSize;
}
