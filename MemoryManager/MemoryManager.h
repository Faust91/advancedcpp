#pragma once

#include "SmallObjectAllocator.h"
#include "SimpleAllocator.h"
#include <map>

namespace FastMemoryManager {

	class MemoryManager
	{
	public:
		MemoryManager();
		~MemoryManager();

		void* MM_NEW(size_t i_size);
		void* MM_NEW_A(size_t i_size, size_t i_length);
		void MM_DELETE(void* i_p, size_t i_size);
		void MM_DELETE_A(void* i_p, size_t i_size, size_t i_length);
		void* MM_ALLOC(size_t i_size);
		void MM_FREE(void* i_p, size_t i_size);

		SmallObjectAllocator m_SOA;
		std::map<void*, size_t> m_awfulSizeMap;
	};

}

#if OVERRIDE_DEFAULT_ALLOCATOR
static FastMemoryManager::MemoryManager fmm{};

void* operator new(std::size_t n);
void operator delete(void* p);
#endif