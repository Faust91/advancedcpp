#include "SmallObjectAllocator.h"
#include <assert.h>

FastMemoryManager::SmallObjectAllocator::SmallObjectAllocator(block_t i_chunkSize)
	: m_chunkSize(i_chunkSize), m_allocFA(nullptr), m_deallocFA(nullptr) {}

void* FastMemoryManager::SmallObjectAllocator::Allocate(block_size_t i_numBytes)
{
	// alloc_FA not set or with wrong block size
	if (m_allocFA == nullptr || m_allocFA->BlockSize() != i_numBytes) {
		// get a FA iterator
		FixedAllocators::iterator i = m_pool.begin();
		// move i to the correct FA
		for (; i != m_pool.end() && i->BlockSize() < i_numBytes; ++i) {}
		// if no correct FA found
		if (i == m_pool.end() || i->BlockSize() > i_numBytes) {
			// create and add a new FA to the pool
			//m_pool.reserve(m_pool.size() + 1);
			i = m_pool.insert(i, FixedAllocator{ i_numBytes, m_chunkSize });
			// update alloc and dealloc FA
			m_allocFA = &*i;
			m_deallocFA = &*i;
		}
		else {
			// found a FA, update alloc FA
			m_allocFA = &*i;
		}
	}
	// be sure there is a correct alloc FA now
	assert(m_allocFA != nullptr);
	assert(m_allocFA->BlockSize() == i_numBytes);
	// allocate through the FA and return
	return m_allocFA->Allocate();
}

void FastMemoryManager::SmallObjectAllocator::Deallocate(void* i_p, size_t i_size)
{
	// dealloc_FA not set or with wrong block size
	if (m_deallocFA == nullptr || m_deallocFA->BlockSize() != i_size) {
		// get a FA iterator
		FixedAllocators::iterator i = m_pool.begin();
		// move i to the correct FA
		for (; i != m_pool.end() && i->BlockSize() < i_size; ++i) {}
		// be sure there is a correct dealloc FA now
		assert(i != m_pool.end() && i->BlockSize() == i_size);
		// update dealloc FA
		m_deallocFA = &*i;
	}
	// be sure there is a correct dealloc FA now
	assert(m_deallocFA != nullptr);
	assert(m_deallocFA->BlockSize() == i_size);
	// deallocate through the FA and return
	m_deallocFA->Deallocate(i_p);

}

